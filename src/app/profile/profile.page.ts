import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ProfileService } from '../shared/services/profile.service';
import { MyprofileService } from '../shared/services/myprofile.service';
import { Profile } from '../shared/models/profile.model';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  myProfile: Profile;
  avatarimg ="url('../../assets/imgs/empty.png')";
  image: string;
  male: boolean;
  female:boolean;
  @ViewChild('fileLoader', {static: true}) fileLoader: ElementRef;
  constructor(private myProfileService: MyprofileService, public toastController: ToastController) { }

  ngOnInit() {
    this.myProfileService.getMyProfile().subscribe(profile=>{
      console.log(profile);
      this.myProfile = profile;
      if(profile.interest){
      this.female = this.myProfile.interest.indexOf('female')>=0;
      this.male = this.myProfile.interest.indexOf('male')>=0;
    
    }else{
      this.myProfile.images=[];
      this.myProfile.interest=[];
    }
    if(this.myProfile.images.length>=1){
      this.avatarimg ='url(' + profile.images[0].url + ')';
    }

    })
  }
  
 async update(){
   this.myProfile.interest =[];
   if(this.female)this.myProfile.interest.push('female');
   if(this.male)this.myProfile.interest.push('male');
   await this.myProfileService.updateMyProfile(this.myProfile);
   this.presentToast();
 }
 addImage(event) {
    const file = event.target.files[0];
    let myReader:FileReader = new FileReader();
    myReader.onloadend = (e) => {
     this.myProfileService.addImageToProfile(myReader.result as string)
  }
  myReader.readAsDataURL(file);

 }
selectInterest(interest){
  console.log('interest');
}
 openImage(){
  const element: HTMLElement = this.fileLoader.nativeElement  as HTMLElement;
  element.click();
 }
 async presentToast() {
  const toast = await this.toastController.create({
    message: 'Profile salvo.',
    duration: 2000
  });
  toast.present();
}


}
