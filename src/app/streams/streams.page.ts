import { Component, OnInit } from '@angular/core';
import { Stream } from 'src/app/shared/models/stream.model';
import { StreamService } from '../shared/services/stream.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-streams',
  templateUrl: './streams.page.html',
  styleUrls: ['./streams.page.scss'],
})
export class StreamsPage implements OnInit {
  streams: Stream[] = [];


  constructor(private router: Router, private streamService: StreamService) {
  }

  ngOnInit() {
    this.streamService.getStreams().subscribe(res => {
      this.streams = res;
    });
  }
  gotoLive(stream: Stream) {
    this.router.navigateByUrl('live/' + stream.uid);
  }

}
