import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Profile } from 'src/app/shared/models/profile.model';
import { StreamService } from 'src/app/shared/services/stream.service';
import { Stream } from 'src/app/shared/models/stream.model';
import { LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { MyprofileService } from '../../shared/services/myprofile.service';
import { GameService } from '../../shared/services/game.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  currentUser: any;
  profiles: Profile[] = [];
  stream: Stream;
  timer = 0;
  interval;
  btnsize='25vw';
  profileIndex: number;
  profileOK=false;
  liveId= this.route.snapshot.params.liveId;
  @ViewChild('singleLike', {static: true}) box: ElementRef;
  constructor(private streamService: StreamService,
              private route: ActivatedRoute,
              private loadingController: LoadingController,
              private auth: AngularFireAuth,
              private myProfileService: MyprofileService,
              private gameService: GameService,
              private router: Router
              ) {
   }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Carregando...'
    });
    await loading.present();
    const prof = this.myProfileService.getMyProfile().getValue();
    this.myProfileService.updateMyProfile({...prof,liveId:this.liveId});
    
    await this.streamService.getStream(this.liveId).subscribe(async res => {
      this.stream = res;
      await this.getProfileList();
    });
    this.auth.user.subscribe(user =>{
      this.currentUser = user;
    });
    loading.dismiss();
  }
  ok(userId: string) {
  //  void this.matchService.giveLike(userId);
  //  this.hiddeCrush = false;
  this.gameService.saveAction(this.currentUser.uid, userId,this.liveId,'like');
    this.box.nativeElement.classList.add('tinLeftOut');
    setTimeout(()=> { 
      this.getOneMore('tinLeftOut'); }, 1000);
  
  }
  no(userId: string) {
    //giveDisLike(from: string, to:string, live: string){
    this.gameService.saveAction(this.currentUser.uid, userId,this.liveId,'dislike');
    this.box.nativeElement.classList.add('openDownRightOut');
    setTimeout(async ()=> { await this.getOneMore('openDownRightOut'); }, 1000);
  }
  async getOneMore(animation){
    
    if(this.profileIndex>=(this.profiles.length-1)){
      await this.getProfileList();
      this.box.nativeElement.classList.remove(animation);
      console.log('recuperar mais profiles' );
    }else{
    this.box.nativeElement.classList.remove(animation);
    this.profileIndex ++;
   
    console.log(this.profileIndex);
  }
  };
  ionViewDidEnter(){
    console.log('')
  }
  ionViewWillEnter(){
    console.log('Will it enter');
   if(!this.profileOK){
     this.getProfileList();
   }
  }
  ionViewWillLeave(){
    console.log('Leave')
  }
async getProfileList(){
  const loading = await this.loadingController.create({
    message: 'Carregando...'
  });
  await loading.present();
  this.profiles =null;
  const prof = this.myProfileService.getMyProfile().getValue();
  if(prof.interest.length == 0){
    this.profileOK = false;
  }else{
  this.profileOK = true;
  this.profiles = await (await this.gameService.getListPotentialMatchs({...prof,liveId:this.liveId},this.stream.dataInicio));
  this.profileIndex = 0;
}
loading.dismiss();
}
gotoProfile(){
  this.router.navigateByUrl('/profile');
}
//   increase() {
//     this.interval = setInterval(() => {
//     if (this.timer >= 1) {
//       this.timer = 0;
//     } else {
//      this.timer = this.timer + 0.05 ;
//   }
//   }, 1000);
// }



}
