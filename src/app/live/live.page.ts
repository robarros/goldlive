import { Component, OnInit, OnChanges, ViewChild, ViewChildren } from '@angular/core';
import { Stream } from '../shared/models/stream.model';
import { StreamService } from '../shared/services/stream.service';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { GameComponent } from './game/game.component';
import { GameService } from '../shared/services/game.service';
import { Profile } from '../shared/models/profile.model';
import { ProfileService } from '../shared/services/profile.service';
import { MyprofileService } from '../shared/services/myprofile.service';
import { XmppService } from '../shared/services/xmpp.service';
import { XmppMessage } from '../shared/models/XmppMessage.model';
// const converse: any;
@Component({
  selector: 'app-live',
  templateUrl: './live.page.html',
  styleUrls: ['./live.page.scss'],
})
export class LivePage implements OnInit , OnChanges {
  url = 'http://localhost:8000/dev.html';
  stream = new Stream();
  iframeShow = false;
  currentUser: any;
  liveId = this.route.snapshot.params.liveId;
  messages: XmppMessage[]=[];
   constructor(private streamService: StreamService,
              private route: ActivatedRoute,
              private gameService: GameService,
              private myprofileService: MyprofileService,
              private loadingController: LoadingController,
              private auth: AngularFireAuth,
              private xmppService: XmppService) {
            
              }
  async ngOnChanges() {

  }
  async ionViewWillEnter(){
   
  }
  async ngOnInit() {
    this.xmppService.init();
    this.xmppService.messages.subscribe(messages=>{
      this.messages = messages;
    })
    // this.iframeShow = true;
    // const loading = await this.loadingController.create({
    //   message: 'Carregando...'
    // });
    // await loading.present();
    // await this.streamService.getStream(this.route.snapshot.params.liveId).subscribe(res => {
    //   loading.dismiss();
    //   this.stream = res;
    //   // this.url = this.stream.streamUrl + this.url;
      
    // });
    // this.auth.user.subscribe(user =>{
    //   this.currentUser = user;
    // });
  }
 
}
