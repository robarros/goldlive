import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivePageRoutingModule } from './live-routing.module';

import { LivePage } from './live.page';
import { GameComponent } from './game/game.component';
import { SafePipe } from '../shared/pipes/safe.pipe';
import { LoginBoxModule } from '../shared/components/login-box/login-box.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivePageRoutingModule,
    LoginBoxModule
  ],
  declarations: [LivePage, GameComponent, SafePipe]
})
export class LivePageModule {}
