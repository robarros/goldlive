import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { StreamService } from './shared/services/stream.service';
import { Stream } from './shared/models/stream.model';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { MyprofileService } from './shared/services/myprofile.service';
import { MatchService } from './shared/services/match.service';
import { Match } from './shared/models/match.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  currentUser: any;
  public appPages = [
    {
      title: 'Profile',
      url: '/profile',
      icon: 'person'
    },
    {
      title: 'Outbox',
      url: '/folder/Outbox',
      icon: 'paper-plane'
    },
    {
      title: 'Favorites',
      url: '/folder/Favorites',
      icon: 'heart'
    },
    {
      title: 'Archived',
      url: '/folder/Archived',
      icon: 'archive'
    },
    {
      title: 'Trash',
      url: '/folder/Trash',
      icon: 'trash'
    },
    {
      title: 'Spam',
      url: '/folder/Spam',
      icon: 'warning'
    }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  streams: Stream[];
  matches: Match[]=[];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private streamService: StreamService,
    public auth: AngularFireAuth,
    private router: Router,
    private myprofileService: MyprofileService,
    private matchService: MatchService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.auth.user.subscribe(user => {
        this.currentUser = user;
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.streamService.getStreams().subscribe(res => {
        this.streams = res;
      });
      this.matchService.get().subscribe(matches =>{
        if(matches){
          this.matches = matches;
      }
      });
       
   

    });
  }
  gotoRoute(route) {
  this.router.navigateByUrl(route);
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  
  }
  copyLink(stream){
    console.log(stream);
  }
}
