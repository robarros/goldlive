import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../shared/services/chat.service';
import {ChatMessage} from '../shared/models/chatmessage.model';
import {Router, ActivatedRoute} from '@angular/router';
import { MatchService } from '../shared/services/match.service';
import { Match } from '../shared/models/match.model';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {


  @ViewChild('content', {static: true}) private content: any;
  @ViewChild('chat_input', {static: true}) messageInput: ElementRef;
  user: any;
  toUser: any;
  editorMsg = '';
  showEmojiPicker = false;
  match: Match; 
  currentUser: any;
  private matchCollection: AngularFirestoreCollection<Match>;
  constructor(
              private matchService: MatchService,
              private route: ActivatedRoute,
              private router: Router,
              public auth: AngularFireAuth,
              db: AngularFirestore) {
                this.matchCollection = db.collection<Match>('matches');
    
  }

  ngOnInit() {
    console.log(this.route.snapshot.params.id);
    this.auth.user.subscribe(user => {
      this.currentUser = user;
    });
    this.matchService.get().subscribe(matches =>{
      if(matches){
        matches.map(mat=>{
          if(mat.uid == this.route.snapshot.params.id){
            this.match = mat;
            this.scrollToBottom();
            console.log(this.match);
          }
        })
    }
    });
  }

  ionViewDidEnter() {
 
  }

  onFocus() {
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }


  /**
   * @name getMsg
   * @returns {Promise<ChatMessage[]>}
   */
  // getMsg() {
  //   // Get mock message list
  //   return this.chatService
  //     .getMsgList()
  //     .subscribe(res => {
  //       if ( res.messages ) {
  //       this.msgList = res.messages;
  //       this.scrollToBottom();
  //       }
  //       });
  // }

  /**
   * @name sendMsg
   */
  sendMsg() {
    if(this.editorMsg=='')return;
    let chat = new ChatMessage();
    chat.userId = this.currentUser.uid;
    chat.message = this.editorMsg;
    this.editorMsg='';
     this.matchCollection.doc(this.route.snapshot.params.id).update(
      {messages: firebase.firestore.FieldValue.arrayUnion(  Object.assign({}, chat))}
     );
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 200);
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  // private setTextareaScroll() {
  //   const textarea = this.messageInput.nativeElement;
  //   textarea.scrollTop = textarea.scrollHeight;
  // }

  goBack() {
    this.router.navigateByUrl('/');
  }
}