import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Profile } from '../models/profile.model';
import { Action } from '../models/action.model';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Stream } from '../models/stream.model';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private actionsCollection: AngularFirestoreCollection<Action>;

  constructor(db: AngularFirestore) {
    this.actionsCollection = db.collection<Action>('actions');
  }


saveAction(from: string, to:string, live: string, type: string){
  let action = new Action();
  action ={...action,
           from: from,
           to: to,
           liveId : live,
           type: type 
          }
  console.log(action);
      return this.actionsCollection.add(action);
}
async getListPotentialMatchs(profile: Profile, entry: Date): Promise<Profile[]> {
  let profiles: Profile[] = [];
  const limit = 5;
  const randomDate = this.randomDate(new Date(2012, 0, 1), entry)

  if(profile.interest.indexOf('female')>=0){
    profiles = await this.queryForPreference(profile, 'female', limit,randomDate);
      }
  if(profile.interest.indexOf('male')>=0){
    const interestprofiles = await this.queryForPreference(profile, 'male', limit, randomDate);
    profiles =[...profiles, ...interestprofiles];

  }
profiles.map(prof=>{
  this.initImages(prof);
})
console.log('profiles');

  return profiles;
}
private async queryForPreference(profile: Profile, preference: string, limit: number, entry: Date): Promise<Profile[]> {
  let profiles: Profile[] = [];
  let snapshot: any = {};
  
  const ts = new firebase.firestore.Timestamp(entry.getTime()/1000,0);
  console.log(ts);
  snapshot = await firebase.firestore().collection('profile')
  .where('interest', 'array-contains' , profile.gender)
  .where('liveId' , '==' ,  profile.liveId)
  .where('gender', '==' , preference)
  .where('updatedAt','>', entry)
  // .orderBy('uid')
  .limit(limit)
  .get();

  snapshot.docs.map(doc => {
    profiles.push({...doc.data(),uid: doc.id});
    
    
  });
  return profiles;
}
initImages(prof: Profile): Profile{
  if(!prof.images)prof.images=[];
  if(prof.images.length==0){
    prof.images.push({
      url:'../../assets/imgs/empty.png',
      fullPath:''
    })
  }
      return prof;
}

randomDate(start: Date, end: Date): Date {
  return new Date(start.getTime() + Math.random() * (new Date().getTime() - start.getTime()));
}

}
