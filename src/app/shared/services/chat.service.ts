import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import {firestore} from 'firebase';
import {Router} from '@angular/router';
import {ProfileService} from './profile.service';
import { ChatMessage } from '../models/chatmessage.model';
import { Match } from '../models/match.model';
import { MatchService } from './match.service';


export class UserInfo {
  id: string;
  name?: string;
  avatar?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  user: any;
  toUser: any;
  matches: Match[] =[];
  match: Match;
  constructor(
              private afs: AngularFirestore,
              private profileService: ProfileService,
              private router: Router,
              private matchService: MatchService) {
                this.matchService.get().subscribe(matches =>{
                    if(matches){
                      this.matches = matches;
                      this.matches.map(mat=>{
                       if(this.match.uid == mat.uid){
                           this.match = mat;
                       }
                        
                      })
                  }
                  });


  }

  getMsgList(): Observable<any> {
    // const msgListUrl = 'https://raw.githubusercontent.com/HsuanXyz/ionic3-chat/master/src/assets/mock/msg-list.json';
    // return this.http.get<any>(msgListUrl)
    //   .pipe(map(response => response.array.map(msg => ({
    //     ...msg
    //   }))));

    return this.afs.doc(this.getMatcheDocPath(this.toUser.match_id))
      .snapshotChanges()
      .pipe(
        map(doc => {
          return { id: doc.payload.id, ...doc.payload.data() };
        })
      );


  }

  sendMsg(msg: ChatMessage) {
    const ref = this.afs.doc(this.getMatcheDocPath(this.toUser.match_id));
    return ref.update({
      messages: firestore.FieldValue.arrayUnion(msg)
    });
  }
  getMatcheDocPath(match_id: string): string {
    return 'eventos/' + this.user.evento_checked_uid + '/matches/' + match_id;
  }

}
