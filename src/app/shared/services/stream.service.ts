import { Injectable } from '@angular/core';
import { Stream } from '../models/stream.model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StreamService {
  private streamsCollection: AngularFirestoreCollection<Stream>;
  private streams: Observable<Stream[]>;
  constructor(db: AngularFirestore) {
    this.streamsCollection = db.collection<Stream>('streams');
    this.streams = this.streamsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const uid = a.payload.doc.id;
          return { uid: uid, ...data };
        });
      })
    );
  }
  getStreams() {
    return this.streams;
  }
  getStream(id) {
    return this.streamsCollection.doc<Stream>(id).valueChanges();
  }
  updateStream(stream: Stream, id: string) {
    return this.streamsCollection.doc(id).update(stream);
  }
  addStream(stream: Stream) {
    return this.streamsCollection.add(stream);
  }
  removeStream(id) {
    return this.streamsCollection.doc(id).delete();
  }
}
