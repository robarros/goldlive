import { TestBed } from '@angular/core/testing';

import { XmppService } from './xmpp.service';

describe('XmppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: XmppService = TestBed.get(XmppService);
    expect(service).toBeTruthy();
  });
});
