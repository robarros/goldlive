import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Profile } from '../models/profile.model';
import { AngularFireAuth } from '@angular/fire/auth';
import { empty, of } from "rxjs";
import {FirestoreService} from './firestore.service'
import { Upload } from '../models/upload.model';

@Injectable({
  providedIn: 'root'
})
export class MyprofileService {
  profile = new BehaviorSubject<Profile>(null);
//  private profile: Observable<Profile>;
  constructor(private db: AngularFirestore, 
                      public auth: AngularFireAuth, 
                      private firestoreService: FirestoreService) {

    this.auth.user.subscribe(user =>{      
      // user.getIdToken(true).then( idToken => {
      //   console.log('user: ',user.uid);
      //   console.log('password:', idToken);
      // })
   
  
      
      if(user){
        const profileDocument = db.doc<Profile>('profile/'+user.uid);
        profileDocument.snapshotChanges().subscribe(a => {
              const data = a.payload.data();
              const uid = a.payload.id;
              this.profile.next({ uid, ...data });
          })
      }else{
        this.profile.next(null);
      }
    })

   }
   getMyProfile() {
    return this.profile;
  }
  updateMyProfile(profile: Profile) {
    const profileDocument = this.db.doc<any>('profile/'+profile.uid);

    const obj = Object.assign({}, profile);
    for (const prop in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, prop)) {
        if (!obj[prop]) {
          delete obj[prop];
        }
      }
    }
    const imgs = profile.images.map(img => Object.assign({}, img));
    obj['images'] = imgs;
    return profileDocument.set(obj);
  }
  async addImageToProfile(base64: string){
    
    const uploadReference = await this.firestoreService.uploadImageBase64(base64, 'profiles');
    let updateProfile = this.profile.getValue();
    const obj = Object.assign({}, uploadReference);
    updateProfile.images = [];
    updateProfile.images.unshift(uploadReference);
    this.updateMyProfile(updateProfile);  
  }
}
