import { Injectable } from '@angular/core';
import {Upload} from '../models/upload.model';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor() {
  }


  uploadImageBase64(base64: string, collection: string): Promise<Upload> {
     base64 = base64.split(',')[1];

    const storageRef = firebase.storage().ref(collection);
    const fileMeta = this.generateNameFromBase64(base64);
    console.log('FileMeta', fileMeta);
    return new Promise((resolve, reject) => {
      const uploadTask = storageRef.child(firebase.auth().currentUser.uid + '/' + fileMeta.fileName )
        .putString(base64, 'base64'  , fileMeta.metadata).then(function (snapshot) {
          snapshot.ref.getDownloadURL().then(url => {
            const uploadresult = new  Upload(snapshot);
            uploadresult.url = url;
            resolve(uploadresult);
          });
      });
    });
  }

  uploadImage(file: File, collection: string): Promise<Upload> {
    const pathFile = collection + '/' + this.generateUUID() + file.name;
    console.log(pathFile);
    const storageRef = firebase.storage().ref();
    return new Promise((resolve, reject) => {
      const uploadTask = storageRef.child(pathFile).put(file).then(function (snapshot) {
        console.log('Uploaded a blob or file!', snapshot);
        resolve(new Upload(snapshot));
      });
    });
  }

  deleteUpload(fullPath: string): Promise<any> {
    if (!fullPath) {
      return new Promise((resolve, reject) => {
        resolve(true);
      });
    }

    const storageRef = firebase.storage().ref();
    const desertRef = storageRef.child(fullPath);
    return new Promise((resolve, reject) => {
      desertRef.delete().then(function () {
        console.log('File deleted successfully');
        resolve(true);
      }).catch(function (error) {
        resolve(false);
      });
    });
  }

  getDownloadURL(imagePath: string): Promise<string> {
    const storageRef = firebase.storage().ref();
    const downloadRef = storageRef.child(imagePath);
    return new Promise((resolve, reject) => {
      downloadRef.getDownloadURL().then(function (url) {
        resolve(url);
      }).catch(function (error) {
        // Handle any errors
      });
    });
  }

  generateUUID(): string { // Public Domain/MIT
    let d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
      d += performance.now();
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });

  }
  generateNameFromBase64(base64: string): any {
    const response: any = {};

    if (base64.startsWith('/')) {
      response.fileName = new Date().getTime() + '.jpg';
      response.metadata = {
        contentType: 'image/jpeg',
      };
    } else if (base64.startsWith('i')) {
      response.fileName = new Date().getTime() + '.png';
      response.metadata = {
        contentType: 'image/png',
      };
    } else if (base64.startsWith('R')) {
      response.fileName = new Date().getTime() + '.gif';
      response.metadata = {
        contentType: 'image/gif',
      };
    }
    return response;
  }
  // dataURLtoFile(dataurl: string) {
  //   const imageName = dataurl.substr(dataurl.lastIndexOf('/') + 1);
  //   console.log(imageName);
  //   const fileToUplad = new File(dataurl);
  //   const arr = dataurl.split(',');
  //   const mime = arr[0].match(/:(.*?);/)[1];
  //   const  bstr = atob(arr[1]);
  //   let n = bstr.length;
  //   const u8arr = new Uint8Array(n);
  //   while ( n -- ) {
  //     u8arr[n] = bstr.charCodeAt(n);
  //   }
  //   return new File([u8arr], filename, {type: mime });
  // }
}
