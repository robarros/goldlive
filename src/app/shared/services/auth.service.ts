import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { ProfileService } from './profile.service';
import { Observable, EMPTY } from 'rxjs';
import { take, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  authState:Observable<any> = null;

  constructor(
    private afAuth: AngularFireAuth,
  ) {  
  } 
  async webFacebookLogin(): Promise<any> {
    console.log('Entrou no WebfacebookLogin');
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('public_profile');
    const result = await this.afAuth.auth.signInWithPopup(provider);
    const credential = result.credential as firebase.auth.OAuthCredential;
    const fbToken = credential.accessToken;
    console.log(credential);
    // const prof = await this.profileService.getProfileFromFireBase();
    // console.log('profile prexistente');
    // if ( !prof ) {
    //   this.profileService.saveProfileFromFB(result.user, fbToken);
    // }
  }
  



}
