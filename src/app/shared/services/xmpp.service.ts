import { Injectable } from '@angular/core';
import {Strophe} from 'strophe.js';
import { $pres} from 'strophe.js';
import { $iq } from 'strophe.js';
import { $msg} from 'strophe.js';
import { $build } from 'strophe.js';
import * as xml2js from 'xml2js';
import { XmppMessage } from '../models/XmppMessage.model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
var Strophethis;

@Injectable({
  providedIn: 'root'
})
export class XmppService {
  BOSH_SERVICE = 'http://localhost:5280/bosh';
  connection: any;
  connectionHold: any;
  messages = new BehaviorSubject<XmppMessage[]>([]);
 status= 'desconectado';
  static connection: any;
  constructor() {
    Strophethis = this;
    this.messages.next([]);

   }

  
  log(msg, data?) {
      // var tr = document.createElement('tr');
      // var th = document.createElement('th');
      // th.setAttribute( "style", "text-align: left; vertical-align: top;" );
      // var td;
  
      // th.appendChild( document.createTextNode(msg) );
      // tr.appendChild( th );
  
      // if (data) {
      //     td = document.createElement('td');
      //     pre = document.createElement('code');
      //     pre.setAttribute("style", "white-space: pre-wrap;");
      //     td.appendChild(pre);
      //     pre.appendChild( document.createTextNode( vkbeautify.xml(data) ) );
      //     tr.appendChild(td);
      // } else {
      //     th.setAttribute('colspan', '2');
      // }
  
      // $('#log').append(tr);
     console.log({msg,...data});
    }
  
  rawInput(data?){
      this.log('RECV', data);
  }
  
  rawOutput(data?){
      this.log('SENT', data);
  }
  
  onConnect(status?){
    // console.log({...this.connection});
     XmppService.connection= this.connection;
    this.connection = Strophethis.connection;
      if (status == Strophe.Status.CONNECTING) {
         console.log('Strophe is connecting.');
      } else if (status == Strophe.Status.CONNFAIL) {
        console.log('Strophe failed to connect.');
      } else if (status == Strophe.Status.DISCONNECTING) {
        console.log('Strophe is disconnecting.');
      } else if (status == Strophe.Status.DISCONNECTED) {
        console.log('Strophe is disconnected.');
        this.status= 'DISCONNECTED';
      } else if (status == Strophe.Status.CONNECTED) {
        console.log('Strophe is connected.');
        console.log('tentando entrar na sala');
        var d = $pres({'from': 'admin@broadsword', 'to': 'teste@conference.broadsword' + '/' + 'ionic2'})
        XmppService.connection.send(d.tree());
        // XmppService.connection.rawOutput = this.rawOutput({});
        XmppService.connection.rawInput = (data) => {
          let parser = new xml2js.Parser(/* options */);
          parser.parseStringPromise(data /*, options */).then((result)=> {
           
            result.body.message.forEach(message=>{
              console.log(message);
              let xmppMsg = new XmppMessage(message);
              Strophethis.messages.next([...Strophethis.messages.getValue(), ...[xmppMsg]])
             }) 
             console.log('messages',Strophethis.messages.getValue());
            })
          .catch(function (err) {
            // Failed
          });
  
  
        };




      }

  }
  init(){
    this.connection = new Strophe.Connection(this.BOSH_SERVICE);
    this.connection.rawInput(this.connection.rawInput);
    this.connection.rawOutput(this.connection.rawOutput);
    this.connection = this.connection.connect(
      "admin@broadsword",
      "#chat2020",
      this.onConnect
      );
  }

disconnect(){
  this.connection.disconnect();
}

}