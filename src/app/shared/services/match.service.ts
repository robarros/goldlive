import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { Observable, BehaviorSubject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, zip, switchMap } from 'rxjs/operators';
import {Match} from '../models/match.model';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class MatchService {
  private matchCollection: AngularFirestoreCollection<any>;
  // private matches= new BehaviorSubject<any[]>(null);
  currentUser:any;
  private matches: Observable<any[]>;
  constructor(db: AngularFirestore,
              public auth: AngularFireAuth) {
               this.matchCollection = db.collection<Match>('matches');
                this.matches = this.auth.user.pipe(
                  switchMap(user =>  
                    db.collection('matches', ref => 
                    ref.where('users', 'array-contains' , user.uid )
                    .orderBy('updatedAt')
                    )
                    .snapshotChanges()
                    .pipe(map(actions=>{
                      return actions.map(a => {
                              const data = a.payload.doc.data();
                              const uid = a.payload.doc.id;
                              this.currentUser = user;
                              return({ uid, ...(data as Object) });
                    })}
                  )
                )));


        //     this.matchCollection = db.collection<any>('matches');
        // this.matchCollection.snapshotChanges().pipe(
        // map(actions => {
        //     console.log(actions);
        //     return actions.map(a => {
        //       const data = a.payload.doc.data();
        //       const uid = a.payload.doc.id;
        //       console.log({ uid, ...data });
              
        //     });
        //   })
        // );
        //   // }})
  }
  get() {
    console.log('get matches');
    return this.matches;
  }
  setMatchVizualized(match: Match){
        console.log(match.uid);
      return this.matchCollection.doc(match.uid).update(
       {deliveredTo: firebase.firestore.FieldValue.arrayUnion(  this.currentUser.uid)}
      );
    }
  
}
