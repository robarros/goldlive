export class ChatMessage {
     userId: string;
     time = new Date().getTime();
     message: string;
     status='pending';
  }