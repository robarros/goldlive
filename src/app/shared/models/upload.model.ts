export class Upload {

    fullPath: string;
    url: string;
    constructor(snapshot: any) {
      if (snapshot.ref.fullPath === 'N/A') {
        this.url = snapshot.ref.url;
      } else {
      this.fullPath = snapshot.ref.fullPath;
      this.url = snapshot.ref.getDownloadURL().then(fullUrl => this.url = fullUrl);
      }
      }
  }
