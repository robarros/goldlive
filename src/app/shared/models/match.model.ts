import { ChatMessage } from "./chatmessage.model";

export class Match {
    users=[];
    avatars=[];
    streamId='';
    messages: ChatMessage[];
    tofrom:any[]=[];
    names :any[]=[];
    deliveredTo =[];
    uid?: string;
    updatedAt?:any;
}

