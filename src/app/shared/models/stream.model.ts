export class Stream {
    public uid: string;
    public titulo: string;
    public streamUrl: string;
    public imagemMenu: any;
    public dataInicio: any;
    public status: string;
}
