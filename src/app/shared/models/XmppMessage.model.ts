import * as xml2js from 'xml2js';

export class XmppMessage {
 from?: string;
 id?: string;
to?: string;
type?: string;
//propriedades omitidas
// xml:lang: "en"
// xmlns: "jabber:client"
//body é um array filho
body?:any[]
constructor(result){
      this.from = result.$.from;
      this.id = result.$.id;
      this.to = result.$.to;
      this.type = result.$.type;
      this.body= result.body;  
}

}