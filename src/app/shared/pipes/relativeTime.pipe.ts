import { Pipe, PipeTransform } from '@angular/core';
import * as dateFns from 'date-fns';
import { ptBR } from 'date-fns/locale';

@Pipe({
  name: 'relativeTime'
})
export class RelativeTimePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return dateFns.formatDistanceToNow(
      new Date(value),
      { locale: ptBR,
                includeSeconds: true
              }
    );
  }

}