import { Component, OnInit, Input } from '@angular/core';
import {Match} from '../../models/match.model'
@Component({
  selector: 'app-match-item',
  templateUrl: './match-item.component.html',
  styleUrls: ['./match-item.component.scss'],
})
export class MatchItemComponent implements OnInit {
  @Input('match') 
  match: Match;

  constructor() { }

  ngOnInit() {
    console.log(this.match);
  }

}
