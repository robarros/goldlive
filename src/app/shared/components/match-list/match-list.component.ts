import { Component, OnInit } from '@angular/core';
import {MatchService} from '../../services/match.service';
import { Match } from '../../models/match.model';
import { AngularFireAuth } from '@angular/fire/auth';
import { mdTransitionAnimation, ToastController } from '@ionic/angular';
@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.scss'],
})
export class MatchListComponent implements OnInit {
  currentUser: any;
  matches: Match[] =[];
  
  constructor(private matchService: MatchService,
              public auth: AngularFireAuth,
              public toastController: ToastController) { }

  ngOnInit() {
    this.auth.user.subscribe(user => {
      this.currentUser = user;
    });
    this.matchService.get().subscribe(matches =>{
      if(matches){
        this.matches = matches;
        this.matches.sort((a,b) => (a.updatedAt < b.updatedAt) ? 1 : ((b.updatedAt < a.updatedAt) ? -1 : 0)); 

        this.matches.map(mat=>{
          if(mat.deliveredTo.indexOf(this.currentUser.uid)<0){
            this.presentMatchToast(mat.names[mat.tofrom[this.currentUser.uid]]);
            console.log('Novo Match!', mat.names[mat.tofrom[this.currentUser.uid]]);
            this.matchService.setMatchVizualized(mat);
          }
        })
    }
    });
  }


  async presentMatchToast(nome: string) {
    const toast = await this.toastController.create({
      message: 'Você tem uma nova paquera! '+ nome !,
      duration: 2000,
      position: 'middle',
      color: "danger",
      buttons: [
        {
          side: 'start',
          icon: 'heart',
          text: '',
          handler: () => {
            console.log('Favorite clicked');
          }
        }, {
          text: 'Conversar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }
}
