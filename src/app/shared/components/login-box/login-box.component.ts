import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
@Component({
  selector: 'app-login-box',
  templateUrl: './login-box.component.html',
  styleUrls: ['./login-box.component.scss'],
})
export class LoginBoxComponent implements OnInit {
  currentUser: any;
  constructor(private authService: AuthService, public auth: AngularFireAuth) {
    
   }

  ngOnInit() {
    this.auth.user.subscribe(user =>{
      this.currentUser = user;
    })
  }

  facebookLogin(){
    this.authService.webFacebookLogin();

  }

  logout(){
    this.auth.auth.signOut();
  }
}
