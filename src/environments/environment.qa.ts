// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBYY6BaI9yhpH5FbqVAmzt7hTjXdqDOFeY",
  authDomain: "goldcrib-qa.firebaseapp.com",
  databaseURL: "https://goldcrib-qa.firebaseio.com",
  projectId: "goldcrib-qa",
  storageBucket: "goldcrib-qa.appspot.com",
  messagingSenderId: "264595586428",
  appId: "1:264595586428:web:56154dd67f9327deb5b8f9",
  measurementId: "G-TBLTS31JT2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
