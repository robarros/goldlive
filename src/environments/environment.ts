// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBk-4xyzmjlfRDL2IDmDJWm8_B3tmPHEHk',
    authDomain: 'goldcrib-dev.firebaseapp.com',
    databaseURL: 'https://goldcrib-dev.firebaseio.com',
    projectId: 'goldcrib-dev',
    storageBucket: 'goldcrib-dev.appspot.com',
    messagingSenderId: '501071692252',
    appId: '1:501071692252:web:5a6a177ecbdcde84'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
